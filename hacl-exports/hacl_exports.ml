open Js_of_ocaml
open Hacl

let bigstring_of_u8_array u8_array =
  Typed_array.Bigstring.of_uint8Array u8_array

let u8_array_of_bigstring bs =
  Typed_array.Bigstring.to_uint8Array bs

let _ =
  Js.export "Sha256"
    (object%js
      method init () =
        Hash.SHA256.init ()

      method update state input =
        Hash.SHA256.update state (bigstring_of_u8_array input)

      method finish state =
        u8_array_of_bigstring (Hash.SHA256.finish state)

      method digest input =
        u8_array_of_bigstring (Hash.SHA256.digest (bigstring_of_u8_array input))

      method hmacWrite key msg buf =
        let key = bigstring_of_u8_array key in
        let msg = bigstring_of_u8_array msg in
        let buf = bigstring_of_u8_array buf in
        Hash.SHA256.HMAC.write ~key ~msg buf

      method hmacDigest key msg =
        let key = bigstring_of_u8_array key in
        let msg = bigstring_of_u8_array msg in
        u8_array_of_bigstring (Hash.SHA256.HMAC.digest ~key ~msg)

      method checksum s =
        let hash = Hacl.Hash.SHA256.(digest (digest (Bigstring.of_string s))) in
        let res = Bytes.make 4 '\000' in
        Bigstring.blit_to_bytes hash 0 res 0 4 ;
        Bytes.to_string res

    end)

let _ =
  Js.export "Sha512"
    (object%js

      method init () =
        Hash.SHA512.init ()

      method update state input =
        Hash.SHA512.update state (bigstring_of_u8_array input)

      method finish state =
        u8_array_of_bigstring (Hash.SHA512.finish state)

      method digest input =
        u8_array_of_bigstring (Hash.SHA512.digest (bigstring_of_u8_array input))

      method hmacWrite key msg buf =
        let key = bigstring_of_u8_array key in
        let msg = bigstring_of_u8_array msg in
        let buf = bigstring_of_u8_array buf in
        Hash.SHA512.HMAC.write ~key ~msg buf

      method hmacDigest key msg =
        let key = bigstring_of_u8_array key in
        let msg = bigstring_of_u8_array msg in
        u8_array_of_bigstring (Hash.SHA512.HMAC.digest ~key ~msg)
   end)

(*let debug f = Printf.ksprintf (fun s -> Firebug.console##log (Js.string s)) f*)

let _ =
  Js.export "Nonce"
    (object%js

      method gen () =
        u8_array_of_bigstring (Rand.gen 24)

    end)

(*let _ =
  Js.export "Secretbox"
    (object%js

      method unsafeOfBytes buf =
        let buf = bigstring_of_u8_array buf in
        u8_array_of_bigstring (Hacl.Secretbox.unsafe_of_bytes buf)

      method blitToBytes buf pos =
        let buf = bigstring_of_u8_array buf in
        u8_array_of_bigstring (Hacl.Secretbox.blit_of_bytes buf pos)

      method genkey () =
        u8_array_of_bigstring (Hacl.Secretbox.genkey ())

      method box cmsg msg nonce key =
        let cmsg = bigstring_of_u8_array cmsg in
        let msg = bigstring_of_u8_array msg in
        let nonce = bigstring_of_u8_array nonce in
        let key = bigstring_of_u8_array key in
        Hacl.Secretbox.box ~cmsg ~msg ~nonce ~key

      method boxOpen cmsg msg nonce key =
        let cmsg = bigstring_of_u8_array cmsg in
        let msg = bigstring_of_u8_array msg in
        let nonce = bigstring_of_u8_array nonce in
        let key = bigstring_of_u8_array key in
        Hacl.Secretbox.box_open ~cmsg ~msg ~nonce ~key

    end)
*)
let _ =
  Js.export "Box"
    (object%js

      method keypair () =
        let (pk, sk) = Hacl.Box.keypair () in
        object%js (_)
          val sk = sk
          val pk = pk
        end

      method dh pk sk =
        Hacl.Box.dh pk sk

      method box cmsg msg nonce k =
        let cmsg = bigstring_of_u8_array cmsg in
        let msg = bigstring_of_u8_array msg in
        let nonce = bigstring_of_u8_array nonce in
        Hacl.Box.box ~k ~nonce ~msg ~cmsg

      method boxOpen cmsg msg nonce k =
        let cmsg = bigstring_of_u8_array cmsg in
        let msg = bigstring_of_u8_array msg in
        let nonce = bigstring_of_u8_array nonce in
        Hacl.Box.box_open ~k ~nonce ~msg ~cmsg

      method unsafeToBytes key =
        u8_array_of_bigstring (Hacl.Box.unsafe_to_bytes key)

      method blitToBytes key pos buf =
        let buf = bigstring_of_u8_array buf in
        Hacl.Box.blit_to_bytes key ~pos buf

      method equal k1 k2 =
        Hacl.Box.equal k1 k2

      method unsafeSkOfBytes buf =
        let buf = bigstring_of_u8_array buf in
        Hacl.Box.unsafe_sk_of_bytes buf

      method unsafePkOfBytes buf =
        let buf = bigstring_of_u8_array buf in
        Hacl.Box.unsafe_pk_of_bytes buf

      method unsafeCkOfBytes buf =
        let buf = bigstring_of_u8_array buf in
        Hacl.Box.unsafe_ck_of_bytes buf

      method ofSeed pos buf =
        let buf = bigstring_of_u8_array buf in
        Hacl.Box.of_seed ~pos buf

      method neuterize sk =
        Hacl.Box.neuterize sk

    end)

let _ =
  Js.export "Sign"
    (object%js

      method keypair () =
        let pk,sk = Sign.keypair () in
        object%js (_)
          val sk = sk
          val pk = pk
        end

      method sign sk msg =
        let msg = bigstring_of_u8_array msg in
        let signature = Bigstring.create Sign.bytes in
        Sign.sign ~signature ~sk ~msg;
        u8_array_of_bigstring signature

      method verify pk msg signature =
        let msg = bigstring_of_u8_array msg in
        let signature = bigstring_of_u8_array signature in
        Sign.verify ~pk ~msg ~signature

    end)
