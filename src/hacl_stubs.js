/* global _HACL */

var _HACL = require("hacl-wasm");

//Provides: obj_data
function obj_data(x) { return x.data }

//Provides: obj_length
function obj_length(x) { return x.data.length }

//Provides: obj_bpe
function obj_bpe(x) { return x.data.BYTES_PER_ELEMENT }

//Provides: wasm_allocate_u8 mutable
//Requires: obj_length, obj_bpe, obj_data
function wasm_allocate_u8(obj) {
  var idx = _HACL._malloc(obj_length(obj) * obj_bpe(obj));
  _HACL.HEAPU8.set(obj_data(obj), idx / obj_bpe(obj));
  return idx;
}

//Provides: hacl_wasm_free mutable
function hacl_wasm_free(idx) {
  return _HACL._free(idx);
}

//Provides: wasm_update_u8 mutable
//Requires: obj_length, hacl_wasm_free
function wasm_update_u8(dest_obj, src_idx) {
  var data = new Uint8Array(_HACL.HEAPU8.buffer, src_idx, obj_length(dest_obj));
  for (var i = 0; i < obj_length(dest_obj); i++) {
    dest_obj.data[i] = data[i];
  }
  hacl_wasm_free(src_idx);
  return 0;
}

//Provides: ml_randombytes
//Requires: obj_length, wasm_allocate_u8, wasm_update_u8
function ml_randombytes(x) {
  var x_idx = wasm_allocate_u8(x);
  _HACL._randombytes(x_idx, obj_length(x));
  wasm_update_u8(x, x_idx);
  return 0;
}

//Provides: ml_Hacl_SHA2_256_init mutable
//Requires: wasm_allocate_u8, wasm_update_u8
function ml_Hacl_SHA2_256_init(state) {
  var state_idx = wasm_allocate_u8(state);
  var ret = _HACL._Hacl_SHA2_256_init(state_idx);
  wasm_update_u8(state, state_idx);
  return ret;
}

//Provides: ml_Hacl_SHA2_256_update mutable
//Requires: wasm_allocate_u8, wasm_update_u8, hacl_wasm_free
function ml_Hacl_SHA2_256_update(state, bytes) {
  var state_idx = wasm_allocate_u8(state);
  var bytes_idx = wasm_allocate_u8(bytes);
  var ret = _HACL._Hacl_SHA2_256_update(state_idx, bytes_idx);
  wasm_update_u8(state, state_idx);
  hacl_wasm_free(bytes_idx);
  return ret;
}

//Provides: ml_Hacl_SHA2_256_update_last mutable
//Requires: wasm_allocate_u8, wasm_update_u8, hacl_wasm_free
function ml_Hacl_SHA2_256_update_last(state, bytes, len) {
  var state_idx = wasm_allocate_u8(state);
  var bytes_idx = wasm_allocate_u8(bytes);
  var ret = _HACL._Hacl_SHA2_256_update_last(state_idx, bytes_idx, len);
  wasm_update_u8(state, state_idx);
  wasm_update_u8(bytes, bytes_idx);
  return ret;
}

//Provides: ml_Hacl_SHA2_256_finish mutable
//Requires: wasm_allocate_u8, wasm_update_u8, hacl_wasm_free
function ml_Hacl_SHA2_256_finish(state, hash) {
  var state_idx = wasm_allocate_u8(state);
  var hash_idx = wasm_allocate_u8(hash);
  var ret = _HACL._Hacl_SHA2_256_finish(state_idx, hash_idx);
  wasm_update_u8(state, state_idx);
  wasm_update_u8(hash, hash_idx);
  return ret;
}

//Provides: ml_Hacl_HMAC_SHA2_256_hmac mutable
//Requires: obj_length, hacl_wasm_free, wasm_allocate_u8, wasm_update_u8
function ml_Hacl_HMAC_SHA2_256_hmac(out, key, msg) {
  var out_idx = wasm_allocate_u8(out);
  var key_idx = wasm_allocate_u8(key);
  var msg_idx = wasm_allocate_u8(msg);
  var ret = _HACL._Hacl_HMAC_SHA2_256_hmac(out_idx, key_idx, obj_length(key), msg_idx, obj_length(msg));
  wasm_update_u8(out, out_idx);
  hacl_wasm_free(key_idx);
  hacl_wasm_free(msg_idx);
  return ret;
}

//Provides: ml_Hacl_SHA2_512_init mutable
//Requires: wasm_allocate_u8, wasm_update_u8
function ml_Hacl_SHA2_512_init(state) {
  var state_idx = wasm_allocate_u8(state);
  var ret = _HACL._Hacl_SHA2_512_init(state_idx);
  wasm_update_u8(state, state_idx);
  return ret;
}

//Provides: ml_Hacl_SHA2_512_update mutable
//Requires: wasm_allocate_u8, wasm_update_u8, hacl_wasm_free
function ml_Hacl_SHA2_512_update(state, bytes) {
  var state_idx = wasm_allocate_u8(state);
  var bytes_idx = wasm_allocate_u8(bytes);
  var ret = _HACL._Hacl_SHA2_512_update(state_idx, bytes_idx);
  wasm_update_u8(state, state_idx);
  hacl_wasm_free(bytes_idx);
  return ret;
}

//Provides: ml_Hacl_SHA2_512_update_last mutable
//Requires: wasm_allocate_u8, wasm_update_u8, hacl_wasm_free
function ml_Hacl_SHA2_512_update_last(state, bytes, len) {
  var state_idx = wasm_allocate_u8(state);
  var bytes_idx = wasm_allocate_u8(bytes);
  var ret = _HACL._Hacl_SHA2_512_update_last(state_idx, bytes_idx, len);
  wasm_update_u8(state, state_idx);
  wasm_update_u8(bytes, bytes_idx);
  return ret;
}

//Provides: ml_Hacl_SHA2_512_finish mutable
//Requires: wasm_allocate_u8, wasm_update_u8, hacl_wasm_free
function ml_Hacl_SHA2_512_finish(state, hash) {
  var state_idx = wasm_allocate_u8(state);
  var hash_idx = wasm_allocate_u8(hash);
  var ret = _HACL._Hacl_SHA2_512_finish(state_idx, hash_idx);
  wasm_update_u8(state, state_idx);
  wasm_update_u8(hash, hash_idx);
  return ret;
}

//Provides: ml_Hacl_Curve25519_crypto_scalarmult mutable
//Requires: hacl_wasm_free, wasm_allocate_u8, wasm_update_u8
function ml_Hacl_Curve25519_crypto_scalarmult(pk, sk, basepoint) {
  var pk_idx = wasm_allocate_u8(pk);
  var sk_idx = wasm_allocate_u8(sk);
  var basepoint_idx = wasm_allocate_u8(basepoint);
  _HACL._Hacl_Curve25519_crypto_scalarmult(pk_idx, sk_idx, basepoint_idx);
  wasm_update_u8(pk, pk_idx);
  hacl_wasm_free(sk_idx);
  hacl_wasm_free(basepoint_idx);
  return;
}

//Provides: ml_NaCl_crypto_secretbox_easy mutable
//Requires: hacl_wasm_free, wasm_allocate_u8, wasm_update_u8
function ml_NaCl_crypto_secretbox_easy(c, m, n, k) {
  var c_idx = wasm_allocate_u8(c);
  var m_idx = wasm_allocate_u8(m);
  var n_idx = wasm_allocate_u8(n);
  var k_idx = wasm_allocate_u8(k);
  _HACL._NaCl_crypto_secretbox_easy(
    c_idx, m_idx, m.data.length - 32, n_idx, k_idx
  );
  wasm_update_u8(c, c_idx);
  hacl_wasm_free(m_idx);
  hacl_wasm_free(n_idx);
  hacl_wasm_free(k_idx);
  return ;
}

//Provides: ml_NaCl_crypto_secretbox_open_detached mutable
//Requires: hacl_wasm_free, wasm_allocate_u8, wasm_update_u8
function ml_NaCl_crypto_secretbox_open_detached(m,c,mac,n,k) {
  var m_idx = wasm_allocate_u8(m);
  var c_idx = wasm_allocate_u8(c);
  var mac_idx = wasm_allocate_u8(mac);
  var n_idx = wasm_allocate_u8(n);
  var k_idx = wasm_allocate_u8(k);
  var ret = _HACL._NaCl_crypto_secretbox_open_detached(m_idx,
    c_idx,
    mac_idx, m.data.length - 32,
    n_idx,
    k_idx)
  hacl_wasm_free(c_idx);
  hacl_wasm_free(m_idx);
  hacl_wasm_free(mac_idx);
  hacl_wasm_free(n_idx);
  hacl_wasm_free(k_idx);
  return ret;
}

//Provides: ml_NaCl_crypto_box_beforenm
//Requires: hacl_wasm_free, wasm_allocate_u8, wasm_update_u8
function ml_NaCl_crypto_box_beforenm(k,pk,sk) {
  var k_idx = wasm_allocate_u8(k);
  var pk_idx = wasm_allocate_u8(pk);
  var sk_idx = wasm_allocate_u8(sk);
  _HACL._NaCl_crypto_box_beforenm(k_idx,pk_idx,sk_idx)
  wasm_update_u8(k, k_idx);
  hacl_wasm_free(pk_idx);
  hacl_wasm_free(sk_idx);
  return;
}

//Provides: ml_NaCl_crypto_box_easy_afternm
//Requires: hacl_wasm_free, wasm_allocate_u8, wasm_update_u8
function ml_NaCl_crypto_box_easy_afternm(c,m,n,k) {
  var c_idx = wasm_allocate_u8(c);
  var m_idx = wasm_allocate_u8(m);
  var n_idx = wasm_allocate_u8(n);
  var k_idx = wasm_allocate_u8(k);
  _HACL._NaCl_crypto_box_easy_afternm(
    c_idx,
    m_idx, m.data.length - 32,
    n_idx, k.idx);
  hacl_wasm_free(c_idx);
  hacl_wasm_free(m_idx);
  hacl_wasm_free(m_idx);
  hacl_wasm_free(k_idx);
  return;
}

//Provides: ml_NaCl_crypto_box_open_easy_afternm
//Requires: hacl_wasm_free, wasm_allocate_u8, wasm_update_u8
function ml_NaCl_crypto_box_open_easy_afternm(m,c,n,k) {
  var m_idx = wasm_allocate_u8(m);
  var c_idx = wasm_allocate_u8(c);
  var n_idx = wasm_allocate_u8(n);
  var k_idx = wasm_allocate_u8(k);
  var ret = _HACL._NaCl_crypto_box_open_easy_afternm(
    m_idx,
    c_idx, c.data.length - 32,
    n_idx, k.idx);
  hacl_wasm_free(m_idx);
  hacl_wasm_free(c_idx);
  hacl_wasm_free(m_idx);
  hacl_wasm_free(k_idx);
  return ret;
}

//Provides: ml_Hacl_Ed25519_secret_to_public mutable
//Requires: hacl_wasm_free, wasm_allocate_u8, wasm_update_u8
function ml_Hacl_Ed25519_secret_to_public(out, secret) {
  var out_idx = wasm_allocate_u8(out);
  var secret_idx = wasm_allocate_u8(secret);
  _HACL._Hacl_Ed25519_secret_to_public(out_idx, secret_idx);
  wasm_update_u8(out, out_idx);
  hacl_wasm_free(secret);
  return;
}

//Provides: ml_Hacl_Ed25519_sign mutable
//Requires: obj_length, hacl_wasm_free, wasm_allocate_u8, wasm_update_u8
function ml_Hacl_Ed25519_sign(signature, sk, msg) {
  var sk_idx = wasm_allocate_u8(sk);
  var msg_idx = wasm_allocate_u8(msg);
  var signature_idx = wasm_allocate_u8(signature);
  _HACL._Hacl_Ed25519_sign(signature_idx, sk_idx, msg_idx, obj_length(msg));
  wasm_update_u8(signature, signature_idx);
  hacl_wasm_free(sk_idx);
  hacl_wasm_free(msg_idx);
  return;
}

//Provides: ml_Hacl_Ed25519_verify
//Requires: hacl_wasm_free, wasm_allocate_u8
function ml_Hacl_Ed25519_verify(pk, msg, signature) {
  var pubkey_idx = wasm_allocate_u8(pk);
  var msg_idx = wasm_allocate_u8(msg);
  var signature_idx = wasm_allocate_u8(signature);
  var ret = _HACL._Hacl_Ed25519_verify(pubkey_idx, msg_idx, msg.data.length, signature_idx);
  hacl_wasm_free(pubkey_idx);
  hacl_wasm_free(msg_idx);
  hacl_wasm_free(signature_idx);
  return ret;
}
