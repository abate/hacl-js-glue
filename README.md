# tezos - hacl-star js glue

`src/hacl_stubs.js` define the interface between the hacl-star wasm
library and the externals expected in the ocaml code

`test/test.js` contains the jasmine js file to test the low level
hacl binding interface. This use the nmp package `hacl-wasm` to
make sure all functions are defined and behaving well

the file `hacl-exports/hacl-exports.ml` is a jsoo interface to
test the high level calls to hacl-star. it use the ocaml hacl-star
module and expect the externals ( defined in `src/hacl_stubs.js` to
be present at compile time )

the file `test/test_ml.js` is a test using the js function exported
by the module hacl-exports ( this is the jsoo module ) to check
if the high level primitives expected in the tezos code are well defined.

