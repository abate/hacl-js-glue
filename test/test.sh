#!/bin/sh

NODE_VERSION=13
EXEC="docker run -u $(id -u) -w /src --rm -v $(pwd)/../:/src node:$NODE_VERSION-alpine3.10"

$EXEC yarn
$EXEC yarn test

echo "Test executed using"
$EXEC node --version
$EXEC yarn --version
