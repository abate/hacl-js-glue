/* eslint-env node, mocha */
/* global _HACL */

function wasm_update_u8(dest_u8, src_idx) {
  var data = new Uint8Array(_HACL.HEAPU8.buffer, src_idx, dest_u8.length);
  for (var i = 0; i < dest_u8.length; i++) {
    dest_u8[i] = data[i];
  }
  wasm_free(src_idx);
  return 0;
}

function wasm_allocate_u8(u8) {
  var idx = _HACL._malloc(u8.length);
  _HACL.HEAPU8.set(u8, idx);
  return idx;
}

function wasm_free(idx) {
  return _HACL._free(idx);
}

var assert = require('assert');

//var wasmInit = require('../src/hacl.js');

let _HACL

//beforeEach(function(done){
//var promise = wasmInit()
//promise.then(function(wasm){
//_HACL = wasm;
//done();
//});
//});

_HACL = require("hacl-wasm");

const SHA2256_BYTES = 32
const SHA2256_BLOCKBYTES = 64
const SHA2256_STATEBYTES = 137 * 4

const SHA2512_BYTES = 64
const SHA2512_BLOCKBYTES = 128
const SHA2512_STATEBYTES = 169 * 8

const fs = require('fs')
const TestDataRaw = fs.readFileSync('test/test_vectors.json');
const TestData = JSON.parse(TestDataRaw);

describe('Module', function() {
  describe('HACL', function() {
    it('Is defined.', function() {
      assert.notEqual(_HACL, undefined);
    });
  });
});

console.log(_HACL)

describe('Curve25519.ecdh', () => {
  _HACL.Curve25519_51.ecdh(new Uint8Array(32), new Uint8Array(32)).then(function (result) {
    // Here result contains an Uint8Array of size 32 with the DH exchange result
    console.log("AA",result)
  });
})


describe('_final(state, output) sha256', () => {
  TestData.forEach(function (data) {
    if ( data.hash == "sha256" ) {
      it('Test input vectors.', () => {
        var expected_output = new Uint8Array(Buffer.from(data.out, 'hex'));

        var state_index = _HACL._malloc(SHA2256_STATEBYTES);
        _HACL._Hacl_SHA2_256_init(state_index);

        data.in.forEach((i) => {
          var input = new Uint8Array(Buffer.from(i.s, i.enc));
          var limit = input.length;
          var position = 0;
          var buffer;
          var buffer_index;
          while ( position + SHA2256_BLOCKBYTES < limit ) {
            buffer = input.slice(position, position + SHA2256_BLOCKBYTES);
            buffer_index = wasm_allocate_u8(buffer);
            _HACL._Hacl_SHA2_256_update(state_index, buffer_index);
            wasm_free(buffer_index)
            position = position + SHA2256_BLOCKBYTES
          }
          var last_limit = limit - position
          if (last_limit > 0 ) {
            buffer = input.slice(position, limit);
            buffer_index = wasm_allocate_u8(buffer);
            _HACL._Hacl_SHA2_256_update_last(state_index, buffer_index, last_limit);
            wasm_free(buffer_index)
          }
        })
        var hash_index = _HACL._malloc(SHA2256_BYTES);
        _HACL._Hacl_SHA2_256_finish(state_index, hash_index);
        var hash = new Uint8Array(_HACL.HEAPU8.buffer, hash_index, SHA2256_BYTES);

        _HACL._free(state_index);
        _HACL._free(hash_index);

        assert.equal(hash.toString('hex'), expected_output.toString('hex'));
      });
    }
  });
});

describe('_final(state, output) sha512', () => {
  TestData.forEach(function (data) {
    if ( data.hash == "sha512" ) {
      it('Test input vectors.', () => {
        var expected_output = new Uint8Array(Buffer.from(data.out, 'hex'));

        var state_index = _HACL._malloc(SHA2512_STATEBYTES);
        _HACL._Hacl_SHA2_512_init(state_index);

        data.in.forEach((i) => {
          var input = new Uint8Array(Buffer.from(i.s, i.enc));
          var limit = input.length;
          var position = 0;
          var buffer;
          var buffer_index;
          while ( position + SHA2512_BLOCKBYTES < limit ) {
            buffer = input.slice(position, position + SHA2512_BLOCKBYTES);
            buffer_index = wasm_allocate_u8(buffer);
            _HACL._Hacl_SHA2_512_update(state_index, buffer_index);
            wasm_free(buffer_index)
            position = position + SHA2512_BLOCKBYTES
          }
          var last_limit = limit - position
          if (last_limit > 0 ) {
            buffer = input.slice(position, limit);
            buffer_index = wasm_allocate_u8(buffer);
            _HACL._Hacl_SHA2_512_update_last(state_index, buffer_index, last_limit);
            wasm_free(buffer_index)
          }
        })
        var hash_index = _HACL._malloc(SHA2512_BYTES);
        _HACL._Hacl_SHA2_512_finish(state_index, hash_index);
        var hash = new Uint8Array(_HACL.HEAPU8.buffer, hash_index, SHA2512_BYTES);

        _HACL._free(state_index);
        _HACL._free(hash_index);

        assert.equal(hash.toString('hex'), expected_output.toString('hex'));
      });
    }
  });
});

describe('sha256_hmac', () => {
  TestData.forEach(function (data) {
    if ( data.hash == "hmac_sha256" ) {
      it('Test input vectors.', () => {
        var expected_output = new Uint8Array(Buffer.from(data.out, 'hex'));

        var key_arr = new Uint8Array(Buffer.from(data.key, 'ascii'));
        var key_index = wasm_allocate_u8(key_arr);
        var msg_arr = new Uint8Array(Buffer.from(data.in.s, data.in.enc));
        var msg_index = wasm_allocate_u8(msg_arr);

        var hash_arr = new Uint8Array(SHA2256_BYTES)
        var hash_index = wasm_allocate_u8(hash_arr);
        _HACL._Hacl_HMAC_SHA2_256_hmac(hash_index,
          key_index, key_arr.length,
          msg_index, msg_arr.length)
        wasm_update_u8(hash_arr, hash_index)

        assert.equal(hash_arr.toString('hex'), expected_output.toString('hex'));
      });
    }
  });
});

describe('_randombytes(buffer, length)', function() {
  it('Produces random bytes.', function() {
    var x = new Uint8Array(32)
    var x_ptr = wasm_allocate_u8(x);
    _HACL._randombytes(x_ptr, x.length);
    wasm_update_u8(x, x_ptr);
    assert.equal(x.length,32)
  });
});

